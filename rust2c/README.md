# Build instuctions

*  Compile C code: 

    `clang increase.c -flto=thin -c -o increase.o -O2`
* Create a static library from the C code:

    `ar crs libincrease.a increase.o`
* Invoke `rustc` with the additional arguments:

    `rustc -Clinker-plugin-lto -L. -Copt-level=2 -Clinker=clang -Clink-arg=-fuse-ld=lld main.rs`