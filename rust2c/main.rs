#[link(name = "increase")]
extern "C" {
    fn increase(input: &mut [i32; 4]);
}

fn main(){
    let mut input: [i32; 4] = [0,1,2,3];
    println!("Array before calling C code: {:?}", input);
    unsafe { increase(&mut input)};
    println!("Array after returning to Rust: {:?}", input);
}