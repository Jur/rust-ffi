#[no_mangle]
pub extern "C" fn increase(input: &mut [u8; 4]) {
    for i in 0..input.len() {
        input[i] += input[i];
    }    
}