# Build instuctions

*  Create static rustlib: 

    `rustc --crate-type=staticlib -Clinker-plugin-lto -Copt-level=2 increase.rs`

* Invoke `rustc` with the addtitional arguments:

    `rustc -Clinker-plugin-lto -L. -Copt-level=2 -Clinker=clang -Clink-arg=-fuse-ld=lld ./main.rs`