#[link(name = "increase")]
extern "C" {
    fn increase(input: &mut [u8; 4]);
}

fn main(){
    let mut input: [u8; 4] = [0,1,2,3];
    println!("Array before calling external Rust code: {:?}", input);
    unsafe { increase(&mut input) };
    println!("Array after returning to Rust: {:?}", input);
}