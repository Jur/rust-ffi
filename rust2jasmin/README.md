# Build instuctions

*  Jasmin to assembly: 

    `../jasmin/compiler/jasminc.native increase.jazz -o increase.s`

* Create object file of assembly code:

    `clang increase.s -flto=thin -c -o increase.o -O2`

* Create static lib:

    `ar crs libincrease.a increase.o`

* Invoke `rustc` with the additional arguments:

    `rustc -Clinker-plugin-lto -L. -Copt-level=2 -Clinker=clang -Clink-arg=-fuse-ld=lld main.rs`