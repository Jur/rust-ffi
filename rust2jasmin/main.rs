#[link(name = "increase")]
extern "C" {
    fn increase(input: &mut [u64; 4]);
}

fn main() {
    let mut input: [u64; 4] = [0,1,2,3];
    println!("Array before calling Jasmin code: {:?}", input);
    unsafe { increase(&mut input) };
    println!("Array after returning to Rust: {:?}", input);
}
